;;; -*- lexical-binding: t; -*-

;; Settings for builtin features

(setq inhibit-startup-screen t)
(setq-default major-mode 'text-mode)

(use-package generic-x
  ;; Enable font locking for many generic buffers like fstab and
  ;; passwd
  :init
  (require 'generic-x))



(add-hook 'read-only-mode-hook #'read-only-buffer-setup)
(add-hook 'after-change-major-mode-hook #'read-only-buffer-setup)
(defun read-only-buffer-setup ()
  ;; mu4e can't handle `font-lock' changes, so exclude it
    (if buffer-read-only
        (progn
          (setq-local show-trailing-whitespace nil))
      (progn
        (setq-local show-trailing-whitespace t))))

(setq-default indicate-empty-lines t)

;; Display the formfeed ^L char as line.
;; Inspired by `http://ergoemacs.org/emacs/emacs_form_feed_section_paging.html'

(let ((table (make-display-table)))
  (aset table ?\^L
        (vconcat (make-list fill-column (make-glyph-code ?─ 'font-lock-comment-face))))
  (setq-default buffer-display-table table))



(use-package doc-view
  :init
  (add-hook 'doc-view-mode-hook 'auto-revert-mode))

(use-package isearch
  :config
  (define-key isearch-mode-map (kbd "<S-insert>") #'yank))

(use-package files
  :init
  (setq create-lockfiles nil)
  ;; auto-save
  (setq auto-save-default t
        delete-by-moving-to-trash t
        auto-save-file-name-transforms
        `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
  ;; backup
  (setq backup-by-copying t
        delete-old-versions t))

(use-package xref)

(setq font-use-system-font t)

(use-package recentf
  :init
  (recentf-mode 1))

;;; Frames
(use-package tool-bar
  :init
  (tool-bar-mode 0))                       ; Removes toolbar buttons

(use-package frame
  :init
  (when (display-graphic-p)
    (scroll-bar-mode 0))
  ;; xdisp
  (setq scroll-conservatively 10000
        scroll-margin 3)
  (setq icon-title-format frame-title-format))

;;; Display
; Dont wrap lines by default
(setq-default truncate-lines t
              truncate-partial-width-windows nil)

;;; Window
(when (version< "26" emacs-version)     ; Not available in old emacs
  (setq split-width-threshold 150
        display-buffer-alist
        `((,(rx bol "*help" (* any))
           (display-buffer-reuse-mode-window)
           (mode helpful-mode help-mode))
          (,(rx bol "*Calendar*" eol) display-buffer-at-bottom))))

;;; Indent
(use-package indent
  :init
  (setq-default indent-tabs-mode nil))

;;; Paren showing
(use-package paren
  :init
  (setq show-paren-delay 0
        show-paren-when-point-in-periphery t
        show-paren-when-point-inside-paren t)
  (show-paren-mode t))                     ; Shows matching parentheses

;;; Server
(with-eval-after-load 'server
  (unless (server-running-p)
    (server-start)))

;;; Electricity
(electric-pair-mode t)                  ; Insert pairs of (), "", [], etc.

;;; Convenience
(setq confirm-kill-emacs 'y-or-n-p
      disabled-command-function nil)
(prefer-coding-system 'utf-8-unix)      ; Use UTF-8 wherever possible.

;;; Fill
(setq sentence-end-double-space nil)    ; Sentences end with a single space.
(setq fill-column 76)                   ; Set fill width.

;;; Visual line mode
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

;;; Customize
(with-eval-after-load 'evil
  (with-eval-after-load 'cus-edit
    (define-key custom-mode-map (kbd "C-d") 'evil-scroll-down)
    (define-key custom-mode-map (kbd "C-u") 'evil-scroll-up)))

(defun insert-date-time-stamp (&optional arg)
  "Insert the current date (and/ or time) at point.

If prefix argument, query for format. Uses `format-time-string'."
  (interactive "P")
  (insert (format-time-string
           (if arg
               (read-from-minibuffer "Format time string: " "%F")
             "%F"))))

(setq-default major-mode 'text-mode)
