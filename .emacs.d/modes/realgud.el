(use-package realgud
  :init
  (defun realgud:pdb
      (&rest args)
    (interactive)
    (load-library "realgud")
    (realgud:pdb args))
  :config
  (evil-make-overriding-map realgud:shortkey-mode-map))
