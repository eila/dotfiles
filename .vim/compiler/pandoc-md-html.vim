" from https://github.com/vim-pandoc/vim-pandoc.git
if exists("current_compiler")
  finish
endif

let current_compiler = "pandoc-md-html"

CompilerSet errorformat="%f",\ line\ %l:\ %m
CompilerSet makeprg=pandoc\ %\ -f\ markdown\ -t\ html\ -s\ -o\ %:r.html
