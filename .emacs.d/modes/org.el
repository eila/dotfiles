;; -*- lexical-binding: t; -*-

;;; org.el --- Org mode setup

(prog1 'org
  (add-to-list 'package-selected-packages 'org)
  ;;; general settings
  (add-hook 'org-mode-hook #'electric-pair-add-dollar-signs)

  ;;; Outline
  (setq org-special-ctrl-a/e t)
  (setq org-adapt-indentation nil ; Do not indent automatically by outline
        org-edit-src-content-indentation 0)
  (setq org-catch-invisible-edits 'show-and-error)
  (setq org-list-demote-modify-bullet '(("-" . "+")
                                        ("+" . "*")
                                        ("*" . "-")
                                        ("1." . "-")
                                        ("1)" . "-")))

  (defun org-use-markdown-style-comments-advice ()
    (setq-local comment-start "> ")
    (setq-local comment-start-skip "^\\s-*[#>]\\(?: \\|$\\)"))
  (advice-add 'org-setup-comments-handling :after #'org-use-markdown-style-comments-advice)

  ;;; faces
  (with-eval-after-load 'org-faces
    (set-face-attribute 'org-block nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-code nil :inherit '(shadow fixed-pitch))
    (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
    (set-face-attribute 'org-formula nil :inherit 'org-table))

  ;;; bookmarks
  (defun eila/add-org-files-to-bookmarks ()
    "Adds all files in `org-directory' to bookmarks"
    (mapc (lambda (file)
            (add-to-list 'bookmark-alist `(,file (filename . ,(concat org-directory "/" file)))))
          (directory-files org-directory nil "\\.org$")))
  (with-eval-after-load 'bookmark
    (when (and (not (file-exists-p bookmark-default-file))
               (file-exists-p org-directory))
      (eila/add-org-files-to-bookmarks)))
  ;; (bookmark-bmenu-surreptitiously-rebuild-list)

  ;;; org-agenda
  (setq org-scheduled-past-days 0 ; Only show scheduled things on the same day
        org-agenda-skip-deadline-if-done t
        org-agenda-skip-scheduled-if-done t
        org-agenda-todo-ignore-scheduled 'future)

  ;;; org-babel
  (setq org-babel-load-languages '((emacs-lisp . t)))

  ;;; org-export
  (setq org-export-default-language "sv"
        org-export-backends '(ascii beamer html icalendar latex md odt)
        org-export-with-section-numbers nil)
  (with-eval-after-load 'ox-latex
    (add-to-list 'org-latex-packages-alist '("swedish" "babel"))
    ;; Improves kerning and whatnot
    (add-to-list 'org-latex-packages-alist '("" "microtype"))
    (add-to-list 'org-latex-classes
                 '("letter" "\\documentclass{letter}"
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                   ("\\paragraph{%s}" . "\\paragraph*{%s}")
                   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

  ;;; org-refile
  (setq org-goto-interface 'outline-path-completion
        org-refile-targets '((nil :maxlevel . 9)
                             (org-agenda-files :maxlevel . 9))
        org-refile-use-outline-path 'file
        org-refile-allow-creating-parent-nodes 'confirm
        org-outline-path-complete-in-steps nil)

  ;;; org-todo
  (setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELED(c)")
                            (sequence "|" "PHONE(p)" "MEETING(m)"))
        org-todo-keyword-faces '(("WAITING" . "DarkOrange")
                                 ("HOLD" . "Darkorange")
                                 ("NEXT" . "blue")
                                 ("PHONE" . "blue")
                                 ("MEETING" . "blue")))

  ;;; Window setup
  (setq org-agenda-window-setup 'current-window
        org-src-window-setup 'current-window)
  (when (fboundp 'org-mode)
    (define-key shortcut-map "a" #'org-agenda)
    (define-key shortcut-map "c" #'org-capture)
    (define-key shortcut-map "l" #'org-store-link)
    (define-key shortcut-map "t" #'org-time-stamp))
  (with-eval-after-load 'org
    (with-eval-after-load 'evil
      (evil-define-key 'normal org-mode-map
        (kbd "RET") 'org-open-at-point)))
  (with-eval-after-load 'org-colview
    (define-key org-columns-map "g" nil)))

;;; org-capture
(defun eila/org-simple-capture ()
  "Capture a simple note"
  (interactive)
  (require 'org-capture)
  (let ((org-capture-templates
         '(("x" "" entry (file "")
            "* %?
:LOGBOOK:
- Created                              %U
:END:"))))
    (org-capture nil "x")))
(define-key shortcut-map (kbd "c") 'eila/org-simple-capture)

(defun my-pomodoro                      ; TODO: Create entry in LOGBOOK drawer
    ()                                  ; TODO: Make sure alert works
  (interactive)
  (if (eq major-mode 'org-mode)
      (call-interactively 'org-clock-in)
    (let ((current-prefix-arg '(4)))
      (call-interactively 'org-clock-in)))
  (when (org-clocking-p)
    (run-at-time
     (* 25 60) nil
     (lambda ()
       (org-clock-out)
       (alert "Ta en paus!" :title "🍅 Pomodoro" :category 'pomodoro)))))

;;; evil-org
;; These provides keybindings for evil for a couple of modes
;;
;; This one is for org mode.
;;
;; Overview: https://raw.githubusercontent.com/Somelauw/evil-org-mode/master/doc/keythemes.org
;;
;; https://github.com/Somelauw/evil-org-mode
(prog1 'evil-org
  (add-to-list 'package-selected-packages 'evil-org)
  (when (fboundp 'evil-org-mode)
    (setq-default evil-org-key-theme
          '(                            ; <, >
            navigation                  ; gh, gj, gk, gl, gH
            textobjects                 ; ae, aE, ar, aR
            calendar                    ; M-h, M-j, M-S-h, M-S-j, C-f
            additional
            ;; shift
            todo                        ; t, T, M-t
            heading                     ; O, M-o
            insert                      ; C-t, C-d
            ))
    (add-hook 'org-mode-hook 'evil-org-mode)))

