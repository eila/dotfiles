(use-package haskell-mode
  :init
  (when (fboundp 'haskell-mode)
    (add-to-list 'auto-mode-alist '("\\(l\\|h\\|cpp\\|c2\\)?\\.hs\\'" . haskell-mode))
    (add-hook 'haskell-mode-hook #'interactive-haskell-mode)))
