;;; mode-line.el ---                                 -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Einar Largenius

;; Author: Einar Largenius;;; Setup for the modeline <einar.largenius@gmail.com>
;; Keywords: faces

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(use-package diminish
  ;; Allows hiding modes from modeline
  :init
  (add-to-list 'package-selected-packages 'diminish))

;;; Mode Line
(column-number-mode t)                  ; Shows column number in the modeline

(set-face-attribute 'vc-conflict-state nil :foreground "red")
(set-face-attribute 'vc-edited-state nil :foreground "orange")

(provide 'setup-mode-line)
;;; mode-line.el ends here
