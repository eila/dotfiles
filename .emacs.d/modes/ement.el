(when (and (not (package-installed-p 'ement))
           (member 'ement package-selected-packages))
  ;; Install and load `quelpa-use-package' from elpa.
  (package-install 'quelpa)

  ;; Install `plz' HTTP library (not on MELPA yet).
  (quelpa '(plz :fetcher github :repo "alphapapa/plz.el"))

  ;; Install Ement.
  (quelpa '(ement :fetcher github :repo "alphapapa/ement.el")))

(with-eval-after-load 'ement-room-list
  (when (fboundp 'evil-mode)
    (evil-make-overriding-map ement-room-list-mode-map)
    (evil-make-overriding-map ement-room-mode-map '(normal))
    (evil-define-key '(normal motion) ement-room-mode-map "q" 'quit-window)
    (evil-define-key '(normal motion) ement-room-mode-map "o" 'ement-room-send-message)))

(setq ement-save-sessions t)

