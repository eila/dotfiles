Konfigurationsfiler
====================

Detta är konfigurationsfiler.

Installation
------------

Klona repot till din hemmapp, hämta med `git clone -n <repo>` och
sedan flytta .git mappen till `~` och skriv över allt i hemkatalogen;
`git reset --hard`.

### Beroenden

Beroenden finns i `Documents/packages.list`. Installera på Fedora med `sudo dnf
install $(< ~/Documents/packages.list)`.

Filer och Mappar
--------------------

| Sökväg                   | Syfte                                                                   |
|--------------------------|-------------------------------------------------------------------------|
| .Xresources              | Diverse parametrar för xorg, påverkar typsnitt                          |
| .bash_profile            | Körs i början vid inloggning med bash                                   |
| .bashrc                  | Körs en gång i början på varje interaktiv skal-session                  |
| .clojure/                | Filer till interaktiv Clojure utveckling                                |
| .config/                 | Inställningar till olika program, ligger i en annan mapp pga prydlighet |
| .config/dunst/           | Visar skrivbordsnotiser                                                 |
| .config/firejail/        | Program som kan köra program med begränsade rättigheter                 |
| .config/i3/              | Fönsterhanterare                                                        |
| .config/i3blocks/        | Panel för skrivbordet                                                   |
| .config/i3status/        | Enkel panel för skrivbordet (Inaktuell)                                 |
| .config/redshift.conf    | Sänker färgtemperatur på skärmen när det blir sent                      |
| .config/rofi/            | Programstartare (Inaktuell)                                             |
| .emacs.d/                | Inställningar till Emacs                                                |
| .git/                    | Detta repo i sig                                                        |
| .gitconfig               | Inställningar för detta repo                                            |
| .gitignore               | Ignorera filer i repot                                                  |
| .local/bin/              | Diverse skript                                                          |
| .local/bin/win/          | Skript för Windows                                                      |
| .mozilla/                | Inställningar för Firefox                                               |
| .mutt/                   | Inställningar för ett e-postprogram (Inaktuell)                         |
| .profile                 | Körs varje inloggning                                                   |
| .vim/                    | Inställningar för Vim                                                   |
| .xsession                | körs efter inloggning med grafisk DM                                    |
| Documents/packages.list  | Alla program som kan installeras (för Fedora)
| Documents/Templates/     | Mallar till olika saker                                                 |
| Documents/Templates/mail | Mall för e-post konfiguration                                           |
| Documents/Templates/pdf/ | Mall för att göra pdfer från md-filer med Pandoc                        |
| Documents/Templates/web/ | Mall för webbsidor                                                      |

Kommentarer kring uppstart
--------------------

De flesta distributioner laddar startup filer i denna ordningen:

Vid inloggning:

- `.profile`, som i sin tur laddar:
  - `.bashrc` (eller annan startupfil för valt skal)

Vid varje start av skalet körs även `.bashrc` igen.

Mer Setup
--------------------

Saker som kan behöva fixas för att allt ska funka.

### Systemet

#### Systemd användning

För att starta en systemd-tjänst kör följande:

```
systemctl --user enable --now <Tjänst>
```

#### Hostnamn

Redigera filen `/etc/hostname`. Uppdatering sker vid nästa omstart.

### Användargränsnitt

#### Ändra tangentbordslayout

Använd t.ex `xfce4-keyboard-settings`

```
localectl set-x11-keymap se pc105 nodeadkeys caps:ctrl_modifier
```

#### Aktivera fingeravtrycksläsare

TODO

### Windows

Detta använder [msys2](https://www.msys2.org/) för att skapa en linux-miljö
på Windows. För att köra igång, gör nedanstående:

- Lägg till miljövariabeln ´%HOME%´ (Sök i startmenyn).
- Installera msys2.
  - Lägg till ´msys2/bin´ till path.
  - `pacman -syu` för att uppdatera.
  - För att msys2 ska ha tillgång till Windows path, lägg till
    miljövariabeln ´MSYS2_PATH_TYPE´ med värdet ´inherit´.

### VIM

Använd git submodules för att installera minpac vilket sedan
installerar med hjälp av ´PackInstall´ plugins.

### Emacs

Ingen mer setup krävs för emacs, men det kan vara bra att bygga från
källkoden för att få senaste versionen. För att bygga Emacs gör följande:

1. Ladda ner emacs från [hemsidan](https://www.gnu.org/software/emacs/download.html#gnu-linux)
   och packa upp.
2. Bygg Emacs med `./configure --prefix=${HOME}/.local` och sedan
   `make`.
3. Och kanske `make install`.

### Firefox

Firefox stödjer flera profiler, där inställningar och historik kan vara
annorlunda. för att ändra profil starta med `firefox -p`.

Några viktiga plugins:

| Plugin                     | Beskrivning                  |
|----------------------------|------------------------------|
| Vimium                     | Bättre defaults än Vim-vixen |
| Ublock Origin              | adblocker                    |
| Privacy Badger             | EFFs plugin                  |
| Facebook/ Google Container | Isolerar webbaktivitet       |

### Syncthing

Starta med: `systemctl --user enable --now syncthing.service`. Sedan
kan gränssnittet kommas åt via http://localhost:8384/.

### Latex

Mycket borde finnas, vissa mindre vanliga paket som är bra att ha
eller som används av org-mode-export. Dessa måste hämtas vid sidan av.

Dessa är:

```
wrapfig capt-of
```
