;;; -*- lexical-binding: t; -*-

;;;; Lisp
(prog1 'lisp-mode
  (setq inferior-lisp-program "sbcl")
  (add-hook 'lisp-mode-hook
            (lambda ()
              (setq-local evil-shift-width 2))))

(prog1 'sly
  (or
   (load "~/.guix-profile/share/common-lisp/source/cl-slynk/sly-autoloads.el" :noerror)
   (load "~/.guix-profile/share/common-lisp/sbcl/slynk/sly-autoloads.el" :noerror)
   (load "~/.config/quicklisp/dists/quicklisp/software/sly-*/sly-autoloads.el" :noerror)) ; TODO: Broken path: fix wildcard
  (setq-default sly-symbol-completion-mode nil) ; Otherwise Sly interferes with completion frameworks
  (with-eval-after-load 'sly
    (define-key sly-editing-mode-map (kbd "C-M-n") #'sly-next-note)
    (define-key sly-editing-mode-map (kbd "C-M-p") #'sly-previous-note)
    (with-eval-after-load 'evil
      (evil-define-key '(motion normal) sly-mode-map "K" #'sly-describe-symbol)
      (add-hook 'sly-popup-buffer-mode-hook #'evil-motion-state))))

;;; emacs-lisp
(prog1 'elisp-mode
  (with-eval-after-load 'evil
    (evil-define-key 'normal emacs-lisp-mode-map
      (kbd "C-m") 'eval-last-sexp)
    (add-hook 'edebug-mode-hook 'evil-normalize-keymaps)))

;;; Lispyville

;; Edit parens (s-expressions) effectively.
;; 
;; Most packages for editing sexps have issues with evil-mode. This is
;; due to the fact that vim wont place point past end of line (eol),
;; unlike emacs which does. This behaviour can be changed by setting
;; `evil-move-beyond-eol'. Many emacs libraries is expecting 
(prog1 'lispyville
  (add-to-list 'package-selected-packages 'lispyville)
  (setq lispyville-key-theme
        '(;; operators               ; d, y, c
          ;; c-w                     ; <insert> C-w
          prettify             ; =
          text-objects         ; <operator> a, l, x, f, c, S
          ;; (atom-movement t)       ; W, B, E
          ;; additional-motions      ; H, L, [, ]
          ;; commentary              ; gc, gy
          ;; slurp/barf-cp           ; <, >
          ;; slurp/barf-lispy        ; <, >
          additional                    ; M-j, M-k, M-t
          ;; additional-insert
          ))
  (with-eval-after-load 'lispyville
    (lispyville-set-key-theme lispyville-key-theme)
    (if (fboundp 'diminish)
        (diminish 'lispyville-mode (if window-system " 🍰" " lispyville")))
    (define-key lispyville-mode-map (kbd "M-k") #'lispyville-backward-up-list)
    (define-key lispyville-mode-map (kbd "M-j") #'lispyville-up-list)
    (define-key lispyville-mode-map (kbd "M-h") #'lispyville-backward-sexp)
    (define-key lispyville-mode-map (kbd "M-l") #'lispyville-forward-sexp)
    (define-key lispyville-mode-map (kbd "M-K") #'lispyville-drag-backward)
    (define-key lispyville-mode-map (kbd "M-J") #'lispyville-drag-forward)
    (define-key lispyville-mode-map (kbd "M-H") #'lispyville-barf)
    (define-key lispyville-mode-map (kbd "M-L") #'lispyville-slurp))
  (when (fboundp 'lispyville-mode)
    (define-globalized-minor-mode global-lispyville-mode
      lispyville-mode lispyville-mode)
    (global-lispyville-mode)))
