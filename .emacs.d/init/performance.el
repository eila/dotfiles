;;; performance.el --- Performance options, mostly related to startup

;;; Commentary:
;; 
;; Via:
;; https://github.com/hlissner/doom-emacs/wiki/FAQ#how-is-dooms-startup-so-fast

;;; Code:

;; Avoid garbage collection during startup. The GC eats up quite a bit
;; of time, easily doubling startup time. The trick is to turn up the
;; memory threshold to prevent it from running:
(setq gc-cons-threshold 402653184
      gc-cons-percentage 0.6)

;; Like we did with the garbage collector, unset
;; file-name-handler-alist too (temporarily). Every file opened and
;; loaded by Emacs will run through this list to check for a proper
;; handler for the file, but during startup, it won’t need any of
;; them. Doom doesn’t, at least.
(defvar original--file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)

;; Then reset those options as late as possible.
(defun restore-startup-variables ()
  "Restore startup variables to the default."
  (setq gc-cons-threshold 16777216
	gc-cons-percentage 0.1)
  (setq file-name-handler-alist original--file-name-handler-alist))

(add-hook 'emacs-startup-hook
	  'restore-startup-variables)

(setq bidi-inhibit-bpa t)
(when (version<= "27.1" emacs-version)
  (global-so-long-mode 1))

