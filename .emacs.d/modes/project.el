(use-package project
  :init
  (define-key 'shortcut-map (kbd "f") #'project-find-file))
