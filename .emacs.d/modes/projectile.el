(use-package projectile
  :diminish projectile-mode
  :disabled
  :init
  ;; TODO: does not work on first try
  (define-key 'shortcut-map "p" #'projectile-command-map)
  (define-key 'shortcut-map "p" #'projectile-find-file)
  (add-to-list 'package-selected-packages 'projectile)
  (setq projectile-completion-system 'ivy
        projectile-switch-project-action (lambda () (dired ".")))
  
  :config
  (projectile-mode t))
    
