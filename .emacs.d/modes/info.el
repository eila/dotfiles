;;;; Info  -*- lexical-binding: t; -*-
(use-package info
  :init
  (with-eval-after-load 'evil
    (evil-define-key '(motion normal) Info-mode-map
      (kbd "C-o") 'Info-history-back
      (kbd ",") 'Info-index-next
      (kbd "n") 'Info-next))
  (with-eval-after-load 'info
    (add-hook 'Info-mode-hook #'variable-pitch-mode)
    (set-face-attribute 'Info-quoted nil
                        :background "white smoke"
                        :box '(:line-width 1 :color "dark gray"))))
