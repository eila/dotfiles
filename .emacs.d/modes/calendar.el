;;; calendar.el --- Setup for builtin mode calendar  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Einar Largenius

;; Author: Einar Largenius <einar.largenius@gmail.com>
;; Keywords: calendar

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package calendar
  :init
  (setq calendar-week-start-day 1)      ; Weeks start on mondays
  (setq calendar-intermonth-text        ; Show week
        '(propertize
	  (format "%2d"
		  (car
		   (calendar-iso-from-absolute
		    (calendar-absolute-from-gregorian (list month day year)))))
	  'font-lock-face 'font-lock-function-name-face)))

(provide 'setup-calendar)
;;; calendar.el ends here
