;;; web.el --- A major mode for web templates        -*- lexical-binding: t; -*-
;;
;; Also see js.el, prog.el and skewer.el

;; web-mode
;;
;; For editing web-templates
(use-package web-mode
  :init
  (when (fboundp 'web-mode)
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))))

(use-package company-web
  :after 'company
  :init
  (with-eval-after-load 'company
    (add-to-list 'company-backends 'company-web-html)))

(use-package web-server
  :init
  (when (fboundp 'ws-start)
    (autoload #'ws-dev-server "web-server" nil t))
  :config
  (defun ws-dev-server (docroot)
    (interactive "DServer root: ")
    (ws-start
     (lambda (request)
       (with-slots (process headers) request
         (let ((path (substring (cdr (assoc :GET headers)) 1)))
           (if (ws-in-directory-p docroot path)
               (if (file-directory-p path)
                   (ws-send-directory-list process
                                           (expand-file-name path docroot) "^[^\.]")
                 (ws-send-file process (expand-file-name path docroot)))
             (ws-send-404 process)))))
     9000
     (get-buffer-create "*ws-log*"))
    (browse-url "localhost:9000/index.html")))
