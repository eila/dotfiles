;;; -*- lexical-binding: t; -*-

;;; Options and plugins for evil-mode

;; Evil (Emacs VI Layer) is a Vim-emulator.

;; - https://github.com/emacs-evil/evil
;; - https://github.com/noctuid/evil-guide

;;; evil
(prog1 'evil
  (setq evil-indent-convert-tabs nil
        evil-move-beyond-eol nil ; t for moving by parenthesis properly
        evil-echo-state nil
        evil-symbol-word-search t
        evil-want-Y-yank-to-eol t
        evil-want-C-u-scroll t
        evil-undo-system 'undo-redo)
  (add-to-list 'package-selected-packages 'evil)
  (when (fboundp 'evil-mode)
    (evil-mode 1)
    (define-key evil-motion-state-map (kbd "RET") nil)
    (define-key evil-motion-state-map "å" #'evil-window-map)
    (define-key evil-emacs-state-map "å" #'evil-window-map)
    (define-key evil-visual-state-map "å" #'evil-window-map)
    (define-key evil-visual-state-map "q" #'evil-exit-visual-state)
    (define-key evil-window-map "å" #'evil-window-next)
    (define-key evil-motion-state-map (kbd "g f") #'find-file-at-point)
    (define-key evil-window-map "f" 'other-frame)
    (define-key evil-window-map "F" 'make-frame-command)
    (define-key evil-visual-state-map "q" nil)
    (define-key evil-normal-state-map (kbd "M-.") nil)
    (define-key shortcut-map "u" 'universal-argument)
    (define-key universal-argument-map (kbd "ä u") 'universal-argument-more)
    (define-key universal-argument-map (kbd "C-c u") 'universal-argument-more)
    (define-key evil-inner-text-objects-map "q" 'evil-inner-double-quote)
    (define-key evil-outer-text-objects-map "q" 'evil-a-double-quote)))

;; Sets escape to "fd" like vim
(prog1 'evil-escape
  (setq evil-escape-key-sequence "fd")
  (add-to-list 'package-selected-packages 'evil-escape)
  (when (fboundp 'evil-escape-mode)
    (evil-escape-mode 1)
    (when (fboundp 'diminish)
        (diminish 'evil-escape-mode))))

;; Use with `ys`, cs or ds, or in visual mode: `S`
(prog1 'evil-surround
  (add-to-list 'package-selected-packages 'evil-surround)
  (when (fboundp 'evil-surround-mode)
    (autoload #'evil-surround-edit "evil-surround" nil t)
    (autoload #'evil-Surround-edit "evil-surround" nil t)
    (autoload #'evil-surround-region "evil-surround" nil t)
    (autoload #'evil-Surround-region "evil-surround" nil t)
    (define-key evil-operator-state-map "s" 'evil-surround-edit)
    (define-key evil-operator-state-map "S" 'evil-Surround-edit)
    (define-key evil-visual-state-map "s" 'evil-surround-region)
    (define-key evil-visual-state-map "S" 'evil-Surround-region)))

(prog1 'evil-lion
  (when (fboundp 'evil-lion-mode)
    (define-key evil-normal-state-map "gl" #'evil-lion-left)
    (define-key evil-normal-state-map "gL" #'evil-lion-right)
    (define-key evil-visual-state-map "gl" #'evil-lion-left)
    (define-key evil-visual-state-map "gL" #'evil-lion-right)))

;; Use with gc<motion> or gcc
(use-package evil-commentary
  :init
  (add-to-list 'package-selected-packages 'evil-commentary)
  (evil-commentary-mode 1)
  :config
  (if (fboundp 'diminish)
      (diminish 'evil-commentary-mode)))

(with-eval-after-load 'evil
  (evil-define-state list
    "List state."
    :tag " <L> "
    :suppress-keymap t
    :entry-hook (evil-list-state-dynamic-remap hl-line-mode))

  (progn
    (define-key evil-list-state-map "j" 'evil-next-line)
    (define-key evil-list-state-map "k" 'evil-previous-line)
    (define-key evil-list-state-map "G" 'evil-goto-line)
    (define-key evil-list-state-map "g" (lookup-key evil-motion-state-map "g"))
    (define-key evil-list-state-map "z" (lookup-key evil-motion-state-map "z"))
    (define-key evil-list-state-map (kbd "C-w") 'evil-window-map)

    (when evil-want-C-u-scroll
      (define-key evil-list-state-map (kbd "C-u") 'evil-scroll-up))
    (when evil-want-C-d-scroll
      (define-key evil-list-state-map (kbd "C-d") 'evil-scroll-down))

    (defvar evil-list-state-dynamic-remaps-alist
      '(("g" "r")
        ("j" "J"))
      "Remap the car of each element in list to the cadr of said element")

    (defun evil-list-state-dynamic-remap ()
      "Remap current mode keys dynamically, if able."
      (dolist (binding evil-list-state-dynamic-remaps-alist)
        (when-let ((command (local-key-binding (car binding))))
          (define-key evil-list-state-local-map (cadr binding) command)))))

  (define-key evil-list-state-map "å" evil-window-map)
  (define-key evil-list-state-map "ä" shortcut-map))



(with-eval-after-load 'evil
  (evil-define-text-object evil-a-dollar
    (count &optional beg end type)
    "Select around double dollar."
    :extend-selection t
    (evil-select-quote ?$ beg end type count t))

  (define-key evil-outer-text-objects-map "$" 'evil-a-dollar)

  (evil-define-text-object evil-inner-dollar
    (count &optional beg end type)
    "Select around double dollar."
    :extend-selection nil
    (evil-select-quote ?$ beg end type count))

  (define-key evil-inner-text-objects-map "$" 'evil-inner-dollar))




;; Default states
(setq evil-list-state-modes '(bookmark-bmenu-mode
                              dired-mode
                              doc-view-mode
                              erc-list-mode
                              ement-room-list-mode
                              gnus-group-mode
                              gnus-summary-mode
                              ibuffer-mode
                              mingus-browse-mode
                              mingus-playlist-mode
                              mu4e-headers-mode
                              occur-mode
                              org-agenda-mode
                              package-menu-mode
                              process-menu-mode
                              xref--xref-buffer-mode
                              ztree-mode))
(setq evil-emacs-state-modes '(5x5-mode
                               archive-mode
                               bbdb-mode
                               biblio-selection-mode
                               blackbox-mode
                               bookmark-edit-annotation-mode
                               browse-kill-ring-mode
                               bubbles-mode
                               bzr-annotate-mode
                               calc-mode
                               cfw:calendar-mode
                               completion-list-mode
                               Custom-mode
                               custom-theme-choose-mode
                               debugger-mode
                               delicious-search-mode
                               desktop-menu-blist-mode
                               desktop-menu-mode
                               diff-mode
                               dun-mode
                               dvc-bookmarks-mode
                               dvc-diff-mode
                               dvc-info-buffer-mode
                               dvc-log-buffer-mode
                               dvc-revlist-mode
                               dvc-revlog-mode
                               dvc-status-mode
                               dvc-tips-mode
                               ediff-mode
                               ediff-meta-mode
                               efs-mode
                               Electric-buffer-menu-mode
                               emms-browser-mode
                               emms-mark-mode
                               emms-metaplaylist-mode
                               emms-playlist-mode
                               ess-help-mode
                               etags-select-mode
                               fj-mode
                               gc-issues-mode
                               gdb-breakpoints-mode
                               gdb-disassembly-mode
                               gdb-frames-mode
                               gdb-locals-mode
                               gdb-memory-mode
                               gdb-registers-mode
                               gdb-threads-mode
                               gist-list-mode
                               git-rebase-mode
                               gomoku-mode
                               google-maps-static-mode
                               jde-javadoc-checker-report-mode
                               magit-cherry-mode
                               magit-diff-mode
                               magit-log-mode
                               magit-log-select-mode
                               magit-popup-mode
                               magit-popup-sequence-mode
                               magit-process-mode
                               magit-reflog-mode
                               magit-refs-mode
                               magit-revision-mode
                               magit-stash-mode
                               magit-stashes-mode
                               magit-status-mode
                               mh-folder-mode
                               monky-mode
                               mpuz-mode
                               mu4e-main-mode
                               mu4e-view-mode
                               notmuch-hello-mode
                               notmuch-search-mode
                               notmuch-show-mode
                               notmuch-tree-mode
                               pdf-outline-buffer-mode
                               pdf-view-mode
                               proced-mode
                               rcirc-mode
                               rebase-mode
                               recentf-dialog-mode
                               reftex-select-bib-mode
                               reftex-select-label-mode
                               reftex-toc-mode
                               simple-mpc-mode
                               sldb-mode
                               slime-inspector-mode
                               slime-thread-control-mode
                               slime-xref-mode
                               snake-mode
                               solitaire-mode
                               sr-buttons-mode
                               sr-mode
                               sr-tree-mode
                               sr-virtual-mode
                               tar-mode
                               tetris-mode
                               tla-annotate-mode
                               tla-archive-list-mode
                               tla-bconfig-mode
                               tla-bookmarks-mode
                               tla-branch-list-mode
                               tla-browse-mode
                               tla-category-list-mode
                               tla-changelog-mode
                               tla-follow-symlinks-mode
                               tla-inventory-file-mode
                               tla-inventory-mode
                               tla-lint-mode
                               tla-logs-mode
                               tla-revision-list-mode
                               tla-revlog-mode
                               tla-tree-lint-mode
                               tla-version-list-mode
                               twittering-mode
                               urlview-mode
                               vc-annotate-mode
                               vc-dir-mode
                               vc-git-log-view-mode
                               vc-hg-log-view-mode
                               vc-svn-log-view-mode
                               vm-mode
                               vm-summary-mode
                               w3m-mode
                               wab-compilation-mode
                               xgit-annotate-mode
                               xgit-changelog-mode
                               xgit-diff-mode
                               xgit-revlog-mode
                               xhg-annotate-mode
                               xhg-log-mode
                               xhg-mode
                               xhg-mq-mode
                               xhg-mq-sub-mode
                               xhg-status-extra-mode
                               undo-tree-visualizer-mode
                               calendar-mode
                               elfeed-search-mode
                               cider-stacktrace-mode
                               image-mode
                               sly-db-mode
                               debugger-mode))
(setq evil-motion-state-modes '(Buffer-menu-mode
                                Info-mode
                                Man-mode
                                apropos-mode
                                calendar-mode
                                cider-browse-ns-mode
                                cider-docview-mode
                                cider-inspector-mode
                                cider-test-report-mode
                                color-theme-mode
                                command-history-mode
                                compilation-mode
                                dictionary-mode
                                elfeed-show-mode
                                elisp-refs-mode
                                ement-room-mode
                                ert-results-mode
                                eww-mode
                                geiser-doc-mode
                                geiser-debug-mode
                                gnus-article-mode
                                help-mode
                                helpful-mode
                                mu4e-view-mode
                                shortdoc-mode
                                skewer-error-mode
                                speedbar-mode
                                undo-tree-visualizer-mode
                                woman-mode))
(setq evil-normal-state-modes '(git-commit-mode))
