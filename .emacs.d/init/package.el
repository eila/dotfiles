;;; Basic settings for packages

;;; use-package
;;
;; A macro for defining package usage. Can configure load and download
;; package.
;;
;; - https://github.com/jwiegley/use-package
(setq use-package-verbose t
      use-package-always-defer t
      use-package-enable-imenu-support t
      package-quickstart t
      package-quickstart-file (concat (expand-file-name "var/" user-emacs-directory) "package-quickstart.el"))
(setq load-prefer-newer t)

(setq package-selected-packages ())

;; "Make ‘use-package’ ready for usage."
(when (< emacs-major-version 27)
  (package-initialize))
;; Declares package archives to check from
(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
  (setq package-archive-priorities '(("melpa-stable" . 2) ("gnu" . 1))))
(add-to-list 'package-selected-packages 'use-package)

;; Make sure package.el has updated its keyring
(when (version< emacs-version "26.2")
  (load (concat user-emacs-directory "var/gnu-elpa-keyring-update-2019.3/gnu-elpa-keyring-update.el")))

(unless (fboundp 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))



;; Via: https://genehack.blog/2020/04/a-bit-of-emacs-advice/
(defvar packages-refreshed nil
  "Flag for whether package lists have been refreshed this session.")

(defun maybe-refresh-packages (&rest _)
  "Call `package-refresh-contents`, unless it has already been
refreshed this session."
  (unless packages-refreshed
    (progn
      (package-refresh-contents)
      (setq packages-refreshed t))))

(advice-add 'package-install :before #'maybe-refresh-packages)



(use-package no-littering
  ;; Prevent packages from littering conifg directory.
  :demand t
  :init
  (add-to-list 'package-selected-packages 'no-littering)
  (setq no-littering-etc-directory
        (expand-file-name "var/" user-emacs-directory))
  (setq no-littering-var-directory
        (expand-file-name "var/" user-emacs-directory)))

;; (defmacro defpackage (name &rest args)
;;   ""
;;   (message "%s" args)
;;   `(prog1 ',name
;;      (require ',name)))

;; (equalp
;;  (macroexpand
;;   '(defpackage no-littering
;;      :demand t))
;;  '(prog1 'no-littering
;;     (require 'no-littering)))

