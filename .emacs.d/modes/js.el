;;;; Javascript
;;
;; See also web.el
(use-package js2-mode
  :mode "\\.js\\'"
  :init (setq js2-mode-assume-strict t
              js2-highlight-level 3)
  :config (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)) 
