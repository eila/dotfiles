#!/bin/sh

export EDITOR=vim
export MAILDIR="$HOME/Maildir"
export SYNCDIR="$HOME/Sync"
export LANG=sv_SE.UTF-8

# set PATH so it includes user's private bin directories
export PATH="\
${HOME}/bin:\
${HOME}/.local/bin:\
${PATH}"

# Guix Vars
if command -v guix
then
    exec shepherd &

    export GUIX_PROFILE="$HOME/.guix-profile"
    [ -f "$GUIX_PROFILE/etc/profile" ] && . "$GUIX_PROFILE/etc/profile"
    [ -d "$GUIX_PROFILE/etc/ssl/certs" ] && export SSL_CERT_DIR="$GUIX_PROFILE/etc/ssl/certs"
fi

# Stop GTK apps from complaining. GTK thinks it needs usability plugins like
# screenreaders.
export NO_AT_BRIDGE=1

[ -f ~/.config/profile.d/"$(hostname)".sh ] && . ~/.config/profile.d/"$(hostname)".sh

[ -f ~/.bashrc ] && . ~/.bashrc

if [ -n "$DISPLAY" ]; then
  # turn off bells
  xset b off

  # Config for X resource database
  xrdb "$HOME"/.Xresources

  # Justera tangentbordslayout
  setxkbmap -variant nodeadkeys -option caps:ctrl_modifier

  # Numlock on
  setleds -D +num

  # Starta X
  #
  # 1:an betyder använd tty1
  #
  # Måste vara längst ner, inget körs efteråt
  if [ "$XDG_VTNR" -eq 1 ] ; then
    exec startx
  fi
fi
