;;; languages.el --- Settings for programming langauges   -*- lexical-binding: t; -*-

(use-package prog-mode
  :init
  (add-hook 'prog-mode-hook
            (if (version<= "27" emacs-version)
                #'display-line-numbers-mode
              #'linum-mode))
  (add-hook 'prog-mode-hook #'hs-minor-mode))

(use-package python
  :init (setq python-shell-interpreter "python3"
              python-check-command "pycodestyle"))

(use-package elpy
  :disabled
  :hook (python-mode . elpy-enable))

(provide 'setup-languages)
;;; prog.el ends here
