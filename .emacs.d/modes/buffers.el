;;; -*- lexical-binding: t; -*-

;; Setup for moving around in files, buffers and windows

;;; Ace-window
;;
;; Jump between windows with a single stroke
(use-package ace-window
  :init (setq aw-scope 'frame
              aw-dispatch-always t)
  (with-eval-after-load 'evil
    (define-key 'evil-window-map "a" #'ace-window))
  ; (ace-window-display-mode t)           ; Show ace-window key in modeline
  :config
  (set-face-attribute 'aw-mode-line-face nil :foreground "red" :weight 'bold :inherit nil))

;;; Alert
;;
;; Show various alerts. Use with `(alert)'.
(use-package alert
  :init (setq alert-default-style 'notifications))

;;; Bookmarks
(use-package bookmark
  :init
  (define-key 'shortcut-map (kbd "p") #'bookmark-jump)
  (setq bookmark-save-flag t))

;;; Ibuffer
;;
;; Interactive mode for managing buffers
(use-package ibuffer
  :init
  (setq ibuffer-saved-filter-groups
        '(("default"
           ("org"   (mode . org-mode))
           ("dired" (mode . dired-mode))
           ("erc"   (mode . erc-mode))
           ("emacs" (or (name . "^\\*scratch\\*$")
                        (name . "^\\*Messages\\*$")))
           ("special" (name .  "^\\*")))))
  ;; Reverse the order of buffers
  (defadvice ibuffer-generate-filter-groups (after reverse-ibuffer-groups ()
                                                   activate)
    (setq ad-return-value (nreverse ad-return-value)))
  (add-hook 'ibuffer-mode-hook
            (lambda ()
              (ibuffer-switch-to-saved-filter-groups "default"))))
