;;; Comint
(use-package shell
  :init
  (with-eval-after-load 'shell
    (define-key shell-mode-map (kbd "C-d") nil)
    (define-key comint-mode-map (kbd "C-d") nil)

    (with-eval-after-load 'company
      (define-key comint-mode-map (kbd "<tab>") #'company-select-next))))

(use-package eshell
  :init
  (with-eval-after-load 'eshell
    (setq eshell-prompt-function
          (lambda ()
            (concat "\n"
                    (abbreviate-file-name (eshell/pwd))
                    (if (= (user-uid) 0) " \n# " " \n$ ")))
          eshell-prompt-regexp "^[^#$\n]* \n[#$] ")))
