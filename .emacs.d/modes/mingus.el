(with-eval-after-load 'mingus

  (defun mingus-5-vol-up ()
    (interactive)
    (dotimes (_ 5) (mingus-vol-up)))

  (defun mingus-5-vol-down ()
    (interactive)
    (dotimes (_ 5) (mingus-vol-down)))

  (define-key mingus-*-map "+" 'mingus-5-vol-up)
  (define-key mingus-*-map "-" 'mingus-5-vol-down))

