;;; Minibuffer settings  -*- lexical-binding: t; -*-
;;
;; Interactive interface for completion inside the minibuffer.

(prog1 'minibuffer
  (setq enable-recursive-minibuffers t))

(prog1 'savehist
  ;; Saves minibuffer history in a file
  (savehist-mode 1))

(prog1 'completion
  (with-eval-after-load 'evil
    (define-key evil-insert-state-map (kbd "C-n") #'completion-at-point))
  (define-key global-map (kbd "C-.") #'comint-dynamic-complete-filename))

(prog1 'vertico
  (add-to-list 'package-selected-packages 'vertico)
  (when (fboundp 'vertico-mode)
    (vertico-mode 1))
  (with-eval-after-load 'vertico
    (define-key vertico-map (kbd "C-d") #'vertico-scroll-up)
    (define-key vertico-map (kbd "C-u") #'vertico-scroll-down)
    (define-key vertico-map (kbd "C-w") #'backward-kill-word)))

(prog1 'orderless
  (add-to-list 'package-selected-packages 'orderless)
  (when (locate-library "orderless")
    (require 'orderless)
    (setq completion-styles '(orderless basic)
          completion-category-overrides '((file (styles basic partial-completion))))))

(prog1 'consult
  ;; Enhanced commands with minibuffer completion
  (add-to-list 'package-selected-packages 'consult)
  (when (fboundp 'consult-line)
    (define-key global-map (kbd "C-s") #'consult-line)
    (define-key shortcut-map (kbd "b") #'consult-buffer)
    (define-key shortcut-map (kbd "m") #'consult-imenu)
    (define-key shortcut-map (kbd "p") #'consult-bookmark)
    (define-key shortcut-map (kbd "s") #'consult-git-grep)
    (when (fboundp 'vertico-mode)
      (setq completion-in-region-function #'consult-completion-in-region))))

(prog1 'marginalia
  (add-to-list 'package-selected-packages 'marginalia)
  (when (fboundp 'marginalia-mode)
    (marginalia-mode 1)))
