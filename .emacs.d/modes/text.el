;;;; Text
(use-package text-mode
  :init
  (add-hook 'text-mode-hook #'auto-fill-mode)
  (add-hook 'text-mode-hook #'flyspell-mode)
  (add-hook 'text-mode-hook #'variable-pitch-mode)
  (add-hook 'text-mode-hook (lambda () (highlight-regexp "\t" 'tab-face))))

(defun electric-pair-add-dollar-signs ()
  (setq-local electric-pair-pairs
              (append electric-pair-pairs '((?$ . ?$)))))

