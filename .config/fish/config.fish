function fish_prompt
	set -l last_status $status
    set -l normal (set_color normal)

	echo
	printf '%s%s%s %s%s@%s%s %s%s%s%s' \
		(set_color $fish_color_status) \
		"[$last_status]" \
		"$normal" \
                \
		(set_color $fish_color_user) \
		(whoami) \
                \
		(set_color $fish_color_host) \
		(prompt_hostname) \
                \
		(set_color $fish_color_cwd) \
		(prompt_pwd) \
		"$normal" \
		(__fish_vcs_prompt)

	echo
	echo "\$ "
end
