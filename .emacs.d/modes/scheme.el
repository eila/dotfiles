;;; -*- lexical-binding: t; -*-

(prog1 'scheme
  (setq scheme-program-name "guile")
  (with-eval-after-load 'scheme
    (define-key scheme-mode-map (kbd "C-c C-c") #'scheme-compile-definition)))

(prog1 'geiser
  (when (and (fboundp 'geiser) (executable-find "guix"))
    (add-hook 'geiser-repl-mode-hook #'ensure-guix-dev-profile)))

(defun ensure-guix-dev-profile ()
  "Prepare guix library for geiser.

If guix is installed and a dev-profile doesn't already exists,
create a new guix profile with guix-source and add it geisers
load path."
  (let ((guix-profile (expand-file-name ".guix-dev-profile" user-emacs-directory)))
    (unless (file-exists-p guix-profile)
      (start-process "Guix geiser shell"
                     "*guix-geiser-shell*"
                     "guix" "shell"
                     "guix" "guile"
                     "-r" guix-profile))
    ;; The second one is compiled load path, `geiser-guile-load-path'
    ;; takes both compiled and source versions.
    (add-to-list 'geiser-guile-load-path (concat guix-profile "/share/guile/site/3.0"))
    (add-to-list 'geiser-guile-load-path (concat guix-profile "/lib/guile/3.0/site-ccache"))))

