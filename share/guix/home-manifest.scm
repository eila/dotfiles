;; This "manifest" file can be passed to 'guix package -m' to reproduce
;; the content of your profile.  This is "symbolic": it only specifies
;; package names.  To reproduce the exact same profile, you also need to
;; capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(specifications->manifest
 (list
;;; Desktop
  "dbus"
  "feh"
  "glibc-locales"
  "redshift"
  "stumpwm-with-slynk"
  "xfce"
;;; Terminals and shells
  "fish"
  "rxvt-unicode"
;;; Development tools
  "emacs-sly" ; <2022-07-17>: Seems to be the only slynk package that includes the manual
  "cl-slynk"
  "guile"
  "rlwrap"
  "sbcl"
  "sbcl-slynk"
  "shellcheck"
;; ;;; Virtualization
;;   "qemu"
;;   "libvirt"
;;   "virt-manager"
;;; Programs
  "clojure"
  "clojure-tools"
  "cowsay"
  "emacs"
  "figlet"
  "git"
  "hunspell"
  "hunspell-dict-en"
;; "hunspell-dict-sv" ; Swedish is not available yet :-(
;; "icecat"
  "isync"
  "mu"                                  ; Maildir utils, includes mu4e
  "ncdu"
  "ncmpcpp"
  "nss-certs"
  "nyxt"
  "pandoc"
  "password-store"
  "rename"
  "syncthing"
  "vim-full"
;; ;;; Latex
;;   "texlive-base"
;;   "texlive-babel-swedish"
;; ;;; Latex for pandoc
;;   "texlive-amsfonts"
;;   "texlive-amsmath"
;;   "texlive-babel"
;;   "texlive-booktabs"
;;   "texlive-generic-iftex"
;;   "texlive-hyperref"
;;   "texlive-latex-fancyvrb"
;;   "texlive-latex-geometry"
;;   "texlive-latex-graphics"
;;   "texlive-listings"
;;   "texlive-lm"
;;   "texlive-setspace"
;;   "texlive-tools"
;;   "texlive-ulem"
;;   "texlive-unicode-math"
;;   "texlive-xcolor"
;; ;;; Latex for emacs-org-export
;;   "texlive-capt-of"
;;   "texlive-fonts-ec"
;;   "texlive-grfext"
;;   "texlive-hyperref"
;;   "texlive-latex-graphics"
;;   "texlive-microtype"
;;   "texlive-ulem"
;;   "texlive-wrapfig"
  ))
