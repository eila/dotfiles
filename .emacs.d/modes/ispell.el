;;; Ispell
;;
;; Use `M-x ispell-change-dictionary' to change language
;;
;; Hunspell is used as it seems its the only spell checking program to
;; support multiple languages.
(use-package ispell
  :init
  (setq ispell-personal-dictionary (substitute-in-file-name "$SYNCDIR/stavning"))
  (with-eval-after-load 'ispell
    (when (executable-find "hunspell")
      (setq ispell-program-name "hunspell") ; Use Hunspell if available.
      (setq ispell-dictionary "en_US,sv_SE") ; Set both Swedish and English dictionaries by default.
      (ispell-set-spellchecker-params)       ; ispell-set-spellchecker-params has to be called before ispell-hunspell-add-multi-dic will work
      (ispell-hunspell-add-multi-dic "en_US,sv_SE"))))

(defun ispell-switch-language ()
  "Switch ispell dictionary between english and swedish."
  (interactive)
  (if (equal ispell-local-dictionary "svenska")
      (ispell-change-dictionary "english")
    (ispell-change-dictionary "svenska")))

(define-key shortcut-map "w" 'ispell-switch-language)
