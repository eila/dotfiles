;;;; Packages for providing help

;;; help
(use-package help
  :init
  (with-eval-after-load 'evil
    (evil-define-key '(motion normal) help-mode-map
      (kbd "C-o") 'help-go-back
      (kbd "u") 'evil-scroll-up
      (kbd "d") 'evil-scroll-down)))

;;; which-key
;;
;; Shows commands
(use-package which-key
  :init
  (add-to-list 'package-selected-packages 'which-key)
  (when (locate-library "which-key")
    (run-with-idle-timer 2 nil #'which-key-mode 1))
  (with-eval-after-load 'which-key
    (if (fboundp 'diminish)
        (diminish 'which-key-mode))))

;;; Helpful
;;
;; https://github.com/Wilfred/helpful
;;
;; Adds more help to describe-* functions
(use-package helpful
  :init
  (add-to-list 'package-selected-packages 'helpful)
  (with-eval-after-load 'evil
    (add-hook 'emacs-lisp-mode-hook
              (lambda ()
                (setq-local evil-lookup-func #'helpful-at-point))))
  (setq counsel-describe-function-function #'helpful-callable
        counsel-describe-variable-function #'helpful-variable)
  (define-key help-map "f" #'helpful-callable)
  (define-key help-map "v" #'helpful-variable)
  (define-key help-map "k" #'helpful-key))

(use-package elisp-demos
  :init
  (when (fboundp 'elisp-demos-advice-helpful-update)
    (with-eval-after-load 'helpful
      (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update))))

(prog1 'eldoc
  (global-eldoc-mode 1)
  (when (fboundp 'diminish)
    (diminish 'eldoc-mode)))

(use-package apropos
  :config
  (define-key help-map "A" #'apropos))
