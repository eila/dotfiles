;;; emms.el --- Setup for Emacs multimedia system    -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Einar Largenius

;; Author: Einar Largenius <einar.largenius@gmail.com>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Disabled as the mpd feature won't update the cache when the media
;; library is too big.

;;; Code:


(use-package emms
  :disabled
  :config
  (emms-all)
  (setq emms-player-list '(emms-player-mpd)
        emms-info-functions '(emms-info-mpd)))

(provide 'setup-emms)
;;; emms.el ends here
