;; -*- lexical-binding: t; -*-

;; Custom command and mode definitions that don't fit elsewhere.

(defun open-journal ()
  "Open default notes file."
  (interactive)
  (find-file org-default-notes-file))
(define-key shortcut-map "j" 'open-journal)

(defun search-journal (regexp)
  "Searches journal for `regexp'."
  (interactive "sSök i journalen: ")
  (rgrep regexp "* .*" org-directory))
(define-key shortcut-map "J" 'search-journal)



(defun calendar-insert-selected-date (&optional omit-day-of-week-p)
    "Insert today's date using the current locale.
  With a prefix argument, the date is inserted without the day of
  the week."
    (interactive "P*")
    (save-excursion
      ;; TODO: Use current locale
      (let ((date (calendar-date-string (calendar-cursor-to-date) nil
                                       omit-day-of-week-p)))
        (other-window 1)
        (insert date))))



(use-package pulse
  :init
  (defun pulse-current-line
      ()
    "Highlight the current line momentarily."
    (interactive)
    (pulse-momentary-highlight-one-line
     (point)
     'pulse-highlight-start-face))
  :config
  (when (version< "27" emacs-version)
    (set-face-attribute 'pulse-highlight-start-face nil :extend t)))



(define-minor-mode double-font-size-mode
  "Doubles the font size for all buffers."
  :global t
  :lighter " x2"
  (if double-font-size-mode
      (set-face-attribute 'default nil :height 190)
    (set-face-attribute 'default nil :height 98)))
