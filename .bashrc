#
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# History {{{

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# }}}

# Visual {{{

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

RED="\\e\\[[1;31m\\]"
GREEN="\\e\\[[1;32m\\]"
BLUE="\\e\\[[1;34m\\]"
RESET="\\e\\[[0m\\]"
export PS1="\\n${GREEN}\\u@\\h ${BLUE}\\w ${RESET}\\n\$ "
export PS2="${RED}> ${RESET}"

# }}}

# Convenience {{{

# If set, the pattern '**' used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable bash completion in interactive shells
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		source /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		source /etc/bash_completion
	# Guix specific bash completion
	# Bash-completion sources ~/.bash_completion.  It installs a dynamic
	# completion loader that searches its own completion files as well
	# as those in ~/.guix-profile and /run/current-system/profile.
	elif [ -f /run/current-system/profile/etc/profile.d/bash_completion.sh ]
	then
		source /run/current-system/profile/etc/profile.d/bash_completion.sh
	fi
fi

# }}}

# alias {{{

alias ls="ls --color=auto"
alias grep="grep --color=auto"

if [ -v abook ]; then
    alias abook='abook --datafile $SYNCDIR/kontakter'
fi
tldr() { curl -s "https://raw.githubusercontent.com/tldr-pages/tldr/master/pages/common/$1.md" ; }

# }}}

# local {{{

if [ -f ~/.bashrc_local ]; then
    source ~/.bashrc_local
fi

# }}}
