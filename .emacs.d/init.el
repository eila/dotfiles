;;; Basic settings

;;; Custom
;; 
;; Custom is a builtin system used for storing settings set using ~M-x
;; customize~. custom.el is used for testing purposes and any elements
;; are to be considered transient or moved to other config files. This
;; is loaded early as not to overwrite settings in other config files.
(setq custom-file (concat user-emacs-directory "init/custom.el"))
(load custom-file t)

;; required for package.el to defer loading a bit
(setq package-enable-at-startup nil)

(load (expand-file-name "init/performance" user-emacs-directory))
(load (expand-file-name "init/package"     user-emacs-directory))
(load (expand-file-name "init/bindings"    user-emacs-directory))
(load (expand-file-name "init/minibuffer"  user-emacs-directory))
(load (expand-file-name "init/evil"        user-emacs-directory))
(load (expand-file-name "init/builtins"    user-emacs-directory))
(load (expand-file-name "init/mode-line"   user-emacs-directory))
(load (expand-file-name "init/private"     user-emacs-directory))
(load (expand-file-name "init/functions"   user-emacs-directory))

;;; Mode settings

(mapc #'load (directory-files (concat user-emacs-directory "modes")
                              :full "\\.el$" t))

(when (file-accessible-directory-p "~/.local/share/emacs/site-lisp")
  (add-to-list 'load-path "~/.local/share/emacs/site-lisp")
  (mapc #'load
        (directory-files "~/.local/share/emacs/site-lisp/" :full "autoloads\\.el$")))
