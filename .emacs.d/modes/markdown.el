;; -*- lexical-binding: t; -*-

;; https://jblevins.org/projects/markdown-mode/
(prog1 'markdown-mode
  (add-to-list 'package-selected-packages 'markdown-mode)
  (when (fboundp 'markdown-mode)
    (add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode)))
  (setq markdown-command "pandoc"))

(defun pandoc-compile-markdown-to-pdf ()
  "Exports the current file to a pdf using pandoc."
  (interactive)
  (shell-command
   (concat "pandoc "
           (buffer-name)
           " -f markdown -o "
           (file-name-base (buffer-name))
           ".pdf")))
