;; Dired
;;
;; The built-in file browser
(use-package dired
  :init
  (setq dired-listing-switches "-ahl --group-directories-first" ; Sets directory first sorting for dired. Sets which flags are used for ls.
	dired-dwim-target t
        wdired-allow-to-change-permissions t)
  :bind
  (:map shortcut-map
	("d" . dired-jump))
  :config
  (add-hook 'dired-mode-hook #'hl-line-mode)
  (define-key dired-mode-map "b" #'bookmark-jump)
  (define-key dired-mode-map "f" #'find-file)
  (autoload 'gnus-dired-attach "gnus-dired")
  (define-key dired-mode-map (kbd "C-c RET C-a") #'gnus-dired-attach)
  (with-eval-after-load 'evil
    (evil-define-key 'list dired-mode-map
      "h" 'dired-up-directory
      "l" 'dired-find-alternate-file))
  (when (locate-library "counsel")
    (define-key dired-mode-map "f" 'counsel-find-file)
    (define-key dired-mode-map "b" 'counsel-bookmark)
    (with-eval-after-load 'evil
      (evil-define-key 'list dired-mode-map
        "f" 'counsel-find-file
        "b" 'counsel-bookmark))))
