(use-package vc
  :init
  (setq version-control t
        vc-follow-symlinks nil)           ; Dont warn when editing a file linked to something under vc-control
  )

(use-package magit
  :defer t
  :init
  (add-to-list 'package-selected-packages 'magit)
  (define-key shortcut-map "g" #'magit-status)
  (define-key shortcut-map "G" #'magit-file-dispatch)
  :config
  (with-eval-after-load 'evil
    (evil-define-key '(normal motion emacs) 'magit-mode-map
      (kbd "C-d") 'evil-scroll-down
      (kbd "C-u") 'evil-scroll-up)))

(prog1 'git-gutter
  ; Show changed lines in the gutter
  (add-to-list 'package-selected-packages 'git-gutter)
  (with-eval-after-load 'git-gutter
    (dolist (face '(git-gutter:added
                    git-gutter:deleted
                    git-gutter:modified))
      (set-face-attribute face nil :background (face-attribute face :foreground))))
  (when (fboundp 'global-git-gutter-mode)
    (diminish 'git-gutter-mode)
    (global-git-gutter-mode +1)))

