;;; mail.el ---                                      -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Einar Largenius

;; Author: Einar Largenius <jaadu@lysator.liu.se>
;; Keywords: mail

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; mu4e is a frontent for mu, a program for indexing emails. mu4e is
;; dependant on mu, which is installed separately.

;;; Code:

(prog1 'mu4e
  (when-let (mu-executable (executable-find "mu"))
    (unless (string-match-p "guix-profile" mu-executable)
      (add-to-list 'load-path (substitute-in-file-name "$HOME/.local/share/emacs/site-lisp/mu4e"))
      (add-to-list 'load-path (substitute-in-file-name "/usr/share/emacs/site-lisp/mu4e")))
    (autoload 'mu4e "mu4e" "" t)
    (autoload 'mu4e-user-agent "mu4e-compose" "" t)
    (define-mail-user-agent 'mu4e-user-agent
      'mu4e~compose-mail
      'message-send-and-exit
      'message-kill-buffer
      'message-send-hook)
    (autoload 'mu4e-user-agent "mu4e")
    (autoload 'mu4e~compose-mail "mu4e")
    (setq mail-user-agent 'mu4e-user-agent
          read-mail-command 'mu4e
          mu4e-headers-show-threads t
          mu4e-confirm-quit nil
          mu4e-view-show-addresses t
          mu4e-headers-auto-update t
          mu4e-headers-include-related nil ; Do not show related posts (from the wrong mailboxes).
          mu4e-attachment-dir "~/Downloads"
          mu4e-compose-dont-reply-to-self t
          message-kill-buffer-on-exit t
          mu4e-use-fancy-chars t
          mu4e-split-view 'horizontal
          mu4e-change-filenames-when-moving t ; To work with mbsync
          shr-inhibit-images t)
    (defvaralias 'mu4e-completing-read-function 'completing-read-function)
    (defvaralias 'mu4e-compose-signature 'message-signature)
    (defvaralias 'mu4e-compose-cite-function 'message-cite-function))
  (with-eval-after-load "mu4e"
    (add-to-list 'mu4e-view-actions '("bview in browser" . mu4e-action-view-in-browser))
    (define-key mu4e-view-mode-map (kbd "C-c C-o") #'mu4e~view-browse-url-from-binding)
    (define-key mu4e-view-mode-map (kbd "M-q") #'gnus-article-fill-long-lines)

    (add-hook 'mu4e-compose-mode-hook #'mu4e-compose-format-flowed-mode)
    (define-key mu4e-compose-mode-map (kbd "C-c M-q") #'mu4e-compose-format-flowed-mode)

    (with-eval-after-load 'evil
      (evil-define-key 'motion mu4e-view-mode-map
        "n" 'mu4e-view-headers-next
        "J" 'mu4e~headers-jump-to-maildir
        ;; try to emulate some of the eww key-bindings
        (kbd "RET") #'mu4e~view-browse-url-from-binding
        (kbd "<tab>") #'shr-next-link
        (kbd "<backtab>") #'shr-previous-link)
      (add-hook 'evil-list-state-entry-hook
                (lambda () (when (equal major-mode 'mu4e-headers-mode)
                             (define-key evil-list-state-local-map "r" nil)))
                95)
      (define-key mu4e-main-mode-map "J" 'mu4e~headers-jump-to-maildir))))

(defcustom mu4e-add-before-field
  :flags
  "Before which field a new field should be added by `mu4e-headers-add-field'.")

;; (assq
;;  mu4e-add-before-field
;;  mu4e-headers-fields)

;; TODO: Make sure add-before-field is being used.
;; TODO: Dynamically load header collection
(defun mu4e-headers-add-field ()
  "Add given field to header view."
  (interactive)
  (let* ((collection '(("Message attachments" . :attachments)
                       ("Blind Carbon-Copy recipients for the message" . :bcc)
                       ("Carbon-Copy recipients for the message" . :cc)
                       ("Date/time when the message was written" . :date)
                       ("Date/time when the message was written." . :human-date)
                       ("Flags for the message" . :flags)
                       ("The sender of the message" . :from)
                       ("Sender of the message if it's not me; otherwise the recipient" . :from-or-to)
                       ("Maildir for this message" . :maildir)
                       ("Mailing list id for this message" . :list)
                       ("Mailing list friendly name for this message" . :mailing-list)
                       ("Message-Id for this message" . :message-id)
                       ("Full filesystem path to the message" . :path)
                       ("Check for the cryptographic signature" . :signature)
                       ("Check the cryptographic decryption status" . :decryption)
                       ("Size of the message" . :size)
                       ("Subject of the message" . :subject)
                       ("Tags for the message" . :tags)
                       ("Subject of the thread" . :thread-subject)
                       ("Recipient of the message" . :to)
                       ("Program used for writing this message" . :user-agent)))
         (field (alist-get
                 (completing-read "Choose: " collection)
                 collection nil nil 'equal))
         (size (read-minibuffer "Width: ")))
    (add-to-list 'mu4e-headers-fields `(,field . ,size))
    (mu4e-headers-rerun-search)))

;; TODO: Use collection
(defun mu4e-headers-remove-field ()
  "Remove given field from header view."
  (interactive)
  (setq mu4e-headers-fields
        (assq-delete-all
         (intern (completing-read "Remove field: " mu4e-headers-fields nil :require-match))
         mu4e-headers-fields))
  (mu4e-headers-rerun-search))

(define-minor-mode mu4e-compose-format-flowed-mode
  "Whether outgoing messages use format flowed or not.

Format flowed means that the recievers email client is
responsible for reflowing incoming messages.

Switches `mu4e-compose-format-flowed'."
  :lighter " fflowed"
  (if mu4e-compose-format-flowed-mode
      (progn
        (setq-local mu4e-compose-format-flowed t)
        (auto-fill-mode -1)
        (visual-line-mode 1))
    (progn
      (setq-local mu4e-compose-format-flowed nil)
      (auto-fill-mode 1)
      (visual-line-mode -1))))

;;; message - Sending Emails
(use-package message
  :init
  (setq message-sendmail-f-is-evil t
        message-from-style 'angles
        message-confirm-send t))

;;; Builtin package for sending smtp-emails
;; TODO: Everything is sent in clean text
(use-package sendmail
  :init
  (setq mail-interactive t))

(provide 'setup-mu4e)
;;; mu4e.el ends here
