;; -*- lexical-binding: t; -*-

(use-package eglot
  :init
  (add-to-list 'package-selected-packages 'eglot)
  (setq eglot-put-doc-in-help-buffer t)
  :config
  (defun eglot-setup ()
    (when (eglot-managed-p)
      (setq-local evil-lookup-func #'eglot-help-at-point
                  evil-goto-definition-functions '(evil-goto-definition-xref)
                  eldoc-echo-area-use-multiline-p nil)))
  (add-hook 'eglot-managed-mode-hook #'eglot-setup))

